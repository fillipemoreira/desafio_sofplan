package br.com.gestaoprocesso.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.gestaoprocesso.dto.ResponsavelDto;
import br.com.gestaoprocesso.dto.ResponsavelFiltroDto;
import br.com.gestaoprocesso.dto.ValidatorDto;
import br.com.gestaoprocesso.entity.Responsavel;
import br.com.gestaoprocesso.enums.Paginacao;
import br.com.gestaoprocesso.repository.ResponsavelRepository;
import br.com.gestaoprocesso.validator.IdNotFoundException;
import br.com.gestaoprocesso.validator.group.PersistValidator;
import br.com.gestaoprocesso.validator.group.UpdateValidator;

@Service
public class ResponsavelService {

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private ResponsavelRepository responsavelRepository;
	
	@Autowired
    private ModelMapper modelMapper;
	
	public ResponsavelDto findById(Long id) {
		Optional<Responsavel> responsavel = responsavelRepository.findById(id);
		if(responsavel.isPresent())
			return convertToDto(responsavel.get(), 1L);
		
		return null;
	}
	
	public ResponsavelDto findByCpf(String cpf) {
		Optional<Responsavel> responsavel = responsavelRepository.findByCpf(cpf);
		if(responsavel.isPresent())
			return convertToDto(responsavel.get(), 1L);
		
		return null;
	}
	
	public ResponsavelDto findByEmail(String email) {
		Optional<Responsavel> responsavel = responsavelRepository.findByEmail(email);
		if(responsavel.isPresent())
			return convertToDto(responsavel.get(), 1L);
		
		return null;
	}

	public List<ResponsavelDto> findAll(ResponsavelFiltroDto filtroDto) {
		PageRequest paginacao = PageRequest.of(getPagina(filtroDto.getPagina()), getTotalRegistros(filtroDto.getQtdRegistros()));
		replaceNullPorBlank(filtroDto);
		Page<Responsavel> page = responsavelRepository.findByFiltros(filtroDto.getNome(), filtroDto.getCpf(), filtroDto.getEmail(), paginacao);
		
		if (!page.getContent().isEmpty()) {
			return page.getContent().stream()
					.map(responsavel -> convertToDto(responsavel, page.getTotalElements()))
					.collect(Collectors.toList());
		}

		return new ArrayList<ResponsavelDto>();
	}
	
	@Transactional
	public Responsavel create(ResponsavelDto responsavelDto) {

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();

		Responsavel responsavel = convertToEntity(responsavelDto);
		Set<ConstraintViolation<Responsavel>> violations = validator.validate(responsavel, PersistValidator.class);

		if(Objects.nonNull(violations) && !violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		}
		
		return responsavelRepository.save(responsavel);
	}
	
	@Transactional
	public void deleteById(Long id) {
		validarResponsavel(id);
		
		responsavelRepository.deleteById(id);
		
	}
	
	@Transactional
	public Responsavel edit(Long id, ResponsavelDto responsavelDto) {
		validarResponsavel(id);
		
		Responsavel responsavelToUpdate = convertToEntity(responsavelDto);
		responsavelToUpdate.setId(id);
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();

		Set<ConstraintViolation<Responsavel>> violations = validator.validate(responsavelToUpdate, UpdateValidator.class);

		if(Objects.nonNull(violations) && !violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		}
		
		return responsavelRepository.save(responsavelToUpdate);
	}

	private void validarResponsavel(Long id) {
		if (Objects.isNull(findById(id))) {
			ValidatorDto validatorDto = new ValidatorDto();

			validatorDto.setMensagem(getTexto("responsavel.inexistente"));
			validatorDto.setArgs(Arrays.asList());
			validatorDto.setErro(getTexto("erro.inexistente"));
			validatorDto.setLocalErro(getTexto("erro.local"));
			
			throw new IdNotFoundException(validatorDto);
		}
	}

	private String getTexto(String param) {
		return messageSource.getMessage(param, null, Locale.getDefault());
	}

	private ResponsavelDto convertToDto(Responsavel responsavel, long qtdTotalRegistros) {
		ResponsavelDto dto = modelMapper.map(responsavel, ResponsavelDto.class);
		dto.setQtdTotalRegistros(qtdTotalRegistros);
		return dto;
	}
	
	private Responsavel convertToEntity(ResponsavelDto responsavelDto) {
		return modelMapper.map(responsavelDto, Responsavel.class);
	}
	
	private void replaceNullPorBlank(ResponsavelFiltroDto filtroDto) {
		if(Objects.isNull(filtroDto.getNome())) {
			filtroDto.setNome("");
		}
		
		if(Objects.isNull(filtroDto.getCpf())) {
			filtroDto.setCpf("");
		}
		
		if(Objects.isNull(filtroDto.getEmail())) {
			filtroDto.setEmail("");
		}
		
	}
	
	private int getPagina(Integer paginaParam) {
		return Optional.ofNullable(paginaParam).orElse(Paginacao.PAGINA.getValor());
	}
	
	private int getTotalRegistros(Integer qtdTotalRegistros) {
		return Optional.ofNullable(qtdTotalRegistros).orElse(Paginacao.TOTAL_REGISTROS.getValor());
	}






	
	
}

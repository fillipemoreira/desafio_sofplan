package br.com.gestaoprocesso.service;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

	@Autowired
	private MessageSource messageSource;

	public String getTexto(String key) {
		return messageSource.getMessage(key, null, Locale.getDefault());
	}
	
	public String getTexto(String key, Integer param) {
		Integer[] parametros = new Integer[1];
		parametros[0] = param;
		return messageSource.getMessage(key, parametros, Locale.getDefault());
	}
	
}

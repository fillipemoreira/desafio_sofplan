package br.com.gestaoprocesso.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestaoprocesso.dto.ResponsavelDto;
import br.com.gestaoprocesso.dto.ResponsavelFiltroDto;
import br.com.gestaoprocesso.service.MessageService;
import br.com.gestaoprocesso.service.ResponsavelService;

@RestController
@RequestMapping("/responsaveis")
public class ResponsavelController {

	@Autowired
	private ResponsavelService responsavelService;
	
	@Autowired
	private MessageService messageService;
	
	@GetMapping(value = "/find-all")
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<List<ResponsavelDto>> findAll(@RequestParam(required=false) Integer pagina,
			@RequestParam(required=false) Integer qtdRegistros,
			@RequestParam(required=false) String nome,
			@RequestParam(required=false) String cpf,
			@RequestParam(required=false) String email) {
		
		return ResponseEntity.ok(responsavelService.findAll(new ResponsavelFiltroDto(pagina, qtdRegistros, nome, cpf, email)));
	}
	
	@GetMapping(value = "/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<ResponsavelDto> findById(@PathVariable("id") Long id) {
		return ResponseEntity.ok(responsavelService.findById(id));
	}
	
	@PostMapping()
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<String> create(@RequestBody ResponsavelDto responsavelDto) {
		responsavelService.create(responsavelDto);
		return ResponseEntity.ok(messageService.getTexto("responsavel.criado"));
	}
	
	@PutMapping(value = "/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<String> edit(@PathVariable("id") Long id, @RequestBody ResponsavelDto responsavelDto) {
		responsavelService.edit(id, responsavelDto);
		return ResponseEntity.ok(messageService.getTexto("responsavel.atualizado"));
	}
	
	@DeleteMapping(value = "/{id}")
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<String> deleteById(@PathVariable("id") Long id) {
		responsavelService.deleteById(id);
		return ResponseEntity.ok(messageService.getTexto("responsavel.deletado"));
	}
	

}

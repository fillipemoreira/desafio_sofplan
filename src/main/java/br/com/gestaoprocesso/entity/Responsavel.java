package br.com.gestaoprocesso.entity;

import static javax.persistence.GenerationType.SEQUENCE;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.gestaoprocesso.validator.CpfExistsConstraint;
import br.com.gestaoprocesso.validator.CpfFieldConstraint;
import br.com.gestaoprocesso.validator.DataNascimentoConstraint;
import br.com.gestaoprocesso.validator.EmailConstraint;
import br.com.gestaoprocesso.validator.EmailExistsConstraint;
import br.com.gestaoprocesso.validator.RequiredFieldConstraint;
import br.com.gestaoprocesso.validator.SizeFieldConstraint;
import br.com.gestaoprocesso.validator.group.PersistValidator;
import br.com.gestaoprocesso.validator.group.UpdateValidator;

@Entity
@Table(name = "Responsavel")
public class Responsavel {

	@Id
	@GeneratedValue(strategy=SEQUENCE, generator="responsavel_sequence")
	@SequenceGenerator(name="responsavel_sequence", sequenceName="responsavel_seq")
	@Column
	private Long id;

	@Column
	@RequiredFieldConstraint(message="{nome.obrigatorio}", erro="{erro.obrigatorio}", localErro="responsavel.nome", args={}, groups= {PersistValidator.class, UpdateValidator.class})
	@SizeFieldConstraint(message = "{nome.size}", max=75, erro="tamanhoMaximo", localErro="responsavel.nome", args={75}, groups= {PersistValidator.class, UpdateValidator.class})
	private String nome;

	@Column
	@RequiredFieldConstraint(message="{cpf.obrigatorio}", erro="obrigatorio", localErro="responsavel.cpf", args={}, groups= {PersistValidator.class, UpdateValidator.class})
	@CpfFieldConstraint(message = "{cpf.size}", max=11, min=11, erro="invalido", localErro="responsavel.cpf", args={11}, groups= {PersistValidator.class, UpdateValidator.class})
	@CpfExistsConstraint(message = "{cpf.duplicado}", erro="duplicado", localErro="responsavel.cpf", args={}, groups=PersistValidator.class)
	private String cpf;

	@Column
	@RequiredFieldConstraint(message="{email.obrigatorio}", erro="obrigatorio", localErro="responsavel.email", args={}, groups= {PersistValidator.class, UpdateValidator.class})
	@EmailConstraint(message="{email.invalido}", erro="invalido", localErro="responsavel.email", args={}, groups= {PersistValidator.class, UpdateValidator.class})
	@SizeFieldConstraint(message = "{email.size}", max=65, erro="tamanhoMaximo", localErro="responsavel.email", args={65}, groups= {PersistValidator.class, UpdateValidator.class})
	@EmailExistsConstraint(message = "{email.duplicado}", erro="duplicado", localErro="responsavel.email", args={}, groups=PersistValidator.class)
	private String email;

	@Column
	@DataNascimentoConstraint(message = "{data.erro}", erro="menorDataAtual", localErro="responsavel.dataNascimento", args={}, groups= {PersistValidator.class, UpdateValidator.class})
	private LocalDate dataNascimento;
	

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}

package br.com.gestaoprocesso.validator;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SizeFieldConstraintValidator implements ConstraintValidator<SizeFieldConstraint, Object> {

	private SizeFieldConstraint constraint;
	@Override
	public void initialize(SizeFieldConstraint constraint) {
		this.constraint = constraint;
	}

	@Override
	public boolean isValid(Object target, ConstraintValidatorContext context) {
		if (Objects.nonNull(target) ) {
			String valor = (String) target;
			if(valor.length() > this.constraint.max()) {
				return false;
			}
			if(valor.length() < this.constraint.min()) {
				return false;
			}
			
		}

		return true;
	}

}

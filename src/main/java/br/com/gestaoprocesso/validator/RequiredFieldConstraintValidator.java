package br.com.gestaoprocesso.validator;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RequiredFieldConstraintValidator implements ConstraintValidator<RequiredFieldConstraint, Object> {

	@Override
	public void initialize(RequiredFieldConstraint constraint) {
	}

	@Override
	public boolean isValid(Object target, ConstraintValidatorContext context) {
		if (Objects.nonNull(target) && !"".equals(((String) target))) {
			return true;
		}

		return false;
	}

}

package br.com.gestaoprocesso.validator;

import br.com.gestaoprocesso.dto.ValidatorDto;

public class IdNotFoundException extends RuntimeException {

	private ValidatorDto validator;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IdNotFoundException(ValidatorDto validator) {
		this.validator = validator;
	}

	public ValidatorDto getValidator() {
		return validator;
	}

}

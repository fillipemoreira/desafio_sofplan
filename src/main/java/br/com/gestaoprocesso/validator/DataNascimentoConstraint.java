package br.com.gestaoprocesso.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;


@Constraint(validatedBy = DataNascimentoConstraintValidator.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DataNascimentoConstraint {

	String message() default "";

	String erro() default "";
	String localErro() default "";
	int[] args() default {};
	
	Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
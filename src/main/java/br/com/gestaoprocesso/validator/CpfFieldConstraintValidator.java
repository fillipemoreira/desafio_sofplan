package br.com.gestaoprocesso.validator;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.gestaoprocesso.util.Util;

public class CpfFieldConstraintValidator implements ConstraintValidator<CpfFieldConstraint, Object> {

	private CpfFieldConstraint constraint;
	@Override
	public void initialize(CpfFieldConstraint constraint) {
		this.constraint = constraint;
	}

	@Override
	public boolean isValid(Object target, ConstraintValidatorContext context) {
		if (Objects.nonNull(target) ) {
			String valor = (String) target;
			if(valor.length() > this.constraint.max()) {
				return false;
			}
			if(valor.length() < this.constraint.min()) {
				return false;
			}
			if(!Util.isNumeric(valor)) {
				return false;
			}
		}

		return true;
	}

}

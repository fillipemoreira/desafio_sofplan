package br.com.gestaoprocesso.validator;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.gestaoprocesso.util.Util;

public class EmailConstraintValidator implements ConstraintValidator<EmailConstraint, Object> {

	@Override
	public void initialize(EmailConstraint constraint) {
	}

	@Override
	public boolean isValid(Object target, ConstraintValidatorContext context) {
		if (Objects.nonNull(target) && Util.isValidEmailAddressRegex((String) target)) {
			return true;
		}
		return false;
	}

}

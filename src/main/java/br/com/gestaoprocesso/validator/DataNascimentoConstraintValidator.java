package br.com.gestaoprocesso.validator;

import java.time.LocalDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DataNascimentoConstraintValidator implements ConstraintValidator<DataNascimentoConstraint, Object> {

	@Override
	public void initialize(DataNascimentoConstraint constraint) {
	}

	@Override
	public boolean isValid(Object target, ConstraintValidatorContext context) {
		LocalDate dataNascimento = (LocalDate) target;
		if (dataNascimento.compareTo(LocalDate.now()) < 0) {
			return true;
		}
		return false;
	}

}

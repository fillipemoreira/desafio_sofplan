package br.com.gestaoprocesso.validator;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.gestaoprocesso.config.SpringContext;
import br.com.gestaoprocesso.dto.ResponsavelDto;
import br.com.gestaoprocesso.service.ResponsavelService;

public class CpfExistsConstraintValidator implements ConstraintValidator<CpfExistsConstraint, Object> {


	@Override
	public void initialize(CpfExistsConstraint constraint) {
	}

	@Override
	public boolean isValid(Object target, ConstraintValidatorContext context) {
		ResponsavelService responsavelService = SpringContext.getSpringBean(ResponsavelService.class);
		
		ResponsavelDto responsavel = responsavelService.findByCpf((String) target);
		if (Objects.isNull(responsavel)) {
			return true;
		}
		return false;
	}


}

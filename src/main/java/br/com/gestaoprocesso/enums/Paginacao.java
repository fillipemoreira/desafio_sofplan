package br.com.gestaoprocesso.enums;

public enum Paginacao {

	PAGINA(0), 
	TOTAL_REGISTROS(10);

	private int valor;

	private Paginacao(int valor) {
		this.valor = valor;
	}

	public int getValor() {
		return valor;
	}

}

package br.com.gestaoprocesso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestaoProcessosApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestaoProcessosApplication.class, args);
	}

}

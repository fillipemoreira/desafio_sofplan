package br.com.gestaoprocesso.dto;

import java.util.List;

public class ValidatorDto {

	private String localErro;
	private String erro;
	private String mensagem;
	private List<Integer> args;

	public String getLocalErro() {
		return localErro;
	}

	public void setLocalErro(String localErro) {
		this.localErro = localErro;
	}

	public String getErro() {
		return erro;
	}

	public void setErro(String erro) {
		this.erro = erro;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public List<Integer> getArgs() {
		return args;
	}

	public void setArgs(List<Integer> args) {
		this.args = args;
	}

}

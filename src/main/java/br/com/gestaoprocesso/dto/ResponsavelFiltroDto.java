package br.com.gestaoprocesso.dto;

public class ResponsavelFiltroDto {

	private Integer pagina;
	private Integer qtdRegistros;
	private String nome;
	private String cpf;
	private String email;

	public ResponsavelFiltroDto() {
		super();
	}

	public ResponsavelFiltroDto(Integer pagina, Integer qtdRegistros, String nome, String cpf, String email) {
		super();
		this.pagina = pagina;
		this.qtdRegistros = qtdRegistros;
		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
	}

	public Integer getPagina() {
		return pagina;
	}

	public void setPagina(Integer pagina) {
		this.pagina = pagina;
	}

	public Integer getQtdRegistros() {
		return qtdRegistros;
	}

	public void setQtdRegistros(Integer qtdRegistros) {
		this.qtdRegistros = qtdRegistros;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}

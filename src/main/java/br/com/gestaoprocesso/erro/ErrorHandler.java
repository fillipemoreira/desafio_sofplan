package br.com.gestaoprocesso.erro;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.PRECONDITION_FAILED;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.gestaoprocesso.dto.ValidatorDto;
import br.com.gestaoprocesso.validator.IdNotFoundException;

@ControllerAdvice
public class ErrorHandler {

	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseBody
	public ResponseEntity<?> processValidationError(ConstraintViolationException ex) {

		List<ValidatorDto> erros = ex.getConstraintViolations().stream().map(violation -> {
			ValidatorDto validatorDto = new ValidatorDto();

			validatorDto.setMensagem(violation.getMessage());
			validatorDto.setArgs(Arrays.asList((Integer) getAttribute(violation, "max")));
			validatorDto.setErro((String) getAttribute(violation, "erro"));
			validatorDto.setLocalErro((String) getAttribute(violation, "localErro"));

			return validatorDto;
		}).collect(Collectors.toList());

		return new ResponseEntity<>(erros, PRECONDITION_FAILED);
	}
	
	@ExceptionHandler(IdNotFoundException.class)
	@ResponseBody
	public ResponseEntity<?> processValidationError(IdNotFoundException ex) {
		return new ResponseEntity<>(Arrays.asList(ex.getValidator()), PRECONDITION_FAILED);
	}
	
	

	private Object getAttribute(ConstraintViolation<?> violation, String atributo) {
		return violation.getConstraintDescriptor().getAttributes().get(atributo);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Erro> erro(Exception e) {
		return new ResponseEntity<Erro>(new Erro(null, "Erro interno do Sistema"), INTERNAL_SERVER_ERROR);
	}

}

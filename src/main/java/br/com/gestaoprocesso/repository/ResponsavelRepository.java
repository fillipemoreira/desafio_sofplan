package br.com.gestaoprocesso.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import br.com.gestaoprocesso.entity.Responsavel;

public interface ResponsavelRepository extends JpaRepository<Responsavel, Long>, JpaSpecificationExecutor<Responsavel>{

	@Query(value = "SELECT r FROM Responsavel r WHERE " + 
			" (UPPER(r.nome) LIKE ('%' || upper(:nome) || '%') ) " +
			" AND (UPPER(r.cpf) LIKE ('%' || upper(:cpf) || '%') ) " +
			" AND (UPPER(r.email) LIKE ('%' || upper(:email) || '%')) ")
	Page<Responsavel> findByFiltros(String nome, String cpf, String email, Pageable pageable);
	
	Optional<Responsavel> findByCpf(String cpf);
	Optional<Responsavel> findByEmail(String email);

}

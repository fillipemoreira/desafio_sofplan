package br.com.gestaoprocesso.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringContext implements ApplicationContextAware {

	private static ApplicationContext context;

	@Autowired
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		SpringContext.context = ctx;
	}
	
	public static <T> T getSpringBean(Class<T> beanClass){
		return (T) context.getBean(beanClass);
	}
	
}

package br.com.gestaoprocesso;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import br.com.gestaoprocesso.dto.ResponsavelDto;
import br.com.gestaoprocesso.dto.ResponsavelFiltroDto;
import br.com.gestaoprocesso.dto.ValidatorDto;
import br.com.gestaoprocesso.entity.Responsavel;
import br.com.gestaoprocesso.service.MessageService;
import br.com.gestaoprocesso.service.ResponsavelService;


@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class GestaoProcessosApplicationTests {

	private static final String CPF_VALIDO_RESPONSAVEL = "12302518745";

	@Autowired
	private ResponsavelService responsavelService; 

	@Autowired
	private MessageService messageService; 

	
	@Test
	public void deveCriticarNomeObrigatorio() {
		// Given
		ResponsavelDto responsavelDto = createResponsavel();
		responsavelDto.setNome(null);
		try {
			// When
			responsavelService.create(responsavelDto);
			// Then
			Assert.assertTrue(false);
		} catch (ConstraintViolationException ex) {
			List<ValidatorDto> erros = getErros(ex);
			Assert.assertTrue(erros.stream().anyMatch(erro->messageService.getTexto("nome.obrigatorio").equals(erro.getMensagem())));
		}
	}
	
	@Test
	public void deveCriticarTamanhoMaximoDoNome() {
		
		ResponsavelDto responsavelDto = createResponsavel();
		responsavelDto.setNome("idiwuaioduiowauoiduawoiudoiwauodiuwaoiuidiwuaioduiowauoiduawoiudoiwauodiuw1s");
		try {
			responsavelService.create(responsavelDto);
			Assert.assertTrue(false);
		} catch (ConstraintViolationException ex) {
			List<ValidatorDto> erros = getErros(ex);
			Assert.assertTrue(erros.stream().anyMatch(erro->messageService.getTexto("nome.size",75).equals(erro.getMensagem())));
		}
		
	}
	
	@Test
	public void deveCriticarCpfObrigatorio() {
		
		ResponsavelDto responsavelDto = createResponsavel();
		responsavelDto.setCpf(null);
		try {
			responsavelService.create(responsavelDto);
			Assert.assertTrue(false);
		} catch (ConstraintViolationException ex) {
			List<ValidatorDto> erros = getErros(ex);
			Assert.assertTrue(erros.stream().anyMatch(erro->messageService.getTexto("cpf.obrigatorio").equals(erro.getMensagem())));
		}
		
	}
	
	@Test
	public void deveCriticarCpfInvalido() {
		
		ResponsavelDto responsavelDto = createResponsavel();
		responsavelDto.setCpf("231");
		try {
			responsavelService.create(responsavelDto);
			Assert.assertTrue(false);
		} catch (ConstraintViolationException ex) {
			List<ValidatorDto> erros = getErros(ex);
			Assert.assertTrue(erros.stream().anyMatch(erro->messageService.getTexto("cpf.size", 11).equals(erro.getMensagem())));
		}
		
	}
	
	@Test
	public void deveCriticarCpfDuplicado() {
		
		ResponsavelDto responsavelDto = createResponsavel();
		ResponsavelDto responsavelDto2 = createResponsavel();
		try {
			responsavelService.create(responsavelDto);
			responsavelService.create(responsavelDto2);
			Assert.assertTrue(false);
		} catch (ConstraintViolationException ex) {
			List<ValidatorDto> erros = getErros(ex);
			Assert.assertTrue(erros.stream().anyMatch(erro->messageService.getTexto("cpf.duplicado").equals(erro.getMensagem())));
		}
		
	}
	
	@Test
	public void deveCriticarEmailObrigatorio() {
		
		ResponsavelDto responsavelDto = createResponsavel();
		responsavelDto.setEmail(null);
		try {
			responsavelService.create(responsavelDto);
			Assert.assertTrue(false);
		} catch (ConstraintViolationException ex) {
			List<ValidatorDto> erros = getErros(ex);
			Assert.assertTrue(erros.stream().anyMatch(erro->messageService.getTexto("email.obrigatorio").equals(erro.getMensagem())));
		}
		
	}
	
	@Test
	public void deveCriticarEmailInvalido() {
		
		ResponsavelDto responsavelDto = createResponsavel();
		responsavelDto.setEmail("dsadsad");
		try {
			responsavelService.create(responsavelDto);
			Assert.assertTrue(false);
		} catch (ConstraintViolationException ex) {
			List<ValidatorDto> erros = getErros(ex);
			Assert.assertTrue(erros.stream().anyMatch(erro->messageService.getTexto("email.invalido").equals(erro.getMensagem())));
		}
		
	}
	
	@Test
	public void deveCriticarTamanhoEmail() {
		
		ResponsavelDto responsavelDto = createResponsavel();
		responsavelDto.setEmail("dsaddssdsdsadsaddsuahiusdahusaduhuishaudhuiashdhdsahidsha@gmail.com");
		try {
			responsavelService.create(responsavelDto);
			Assert.assertTrue(false);
		} catch (ConstraintViolationException ex) {
			List<ValidatorDto> erros = getErros(ex);
			Assert.assertTrue(erros.stream().anyMatch(erro->messageService.getTexto("email.size",65).equals(erro.getMensagem())));
		}
		
	}
	
	@Test
	public void deveCriticarEmailDuplicado() {
		
		ResponsavelDto responsavelDto = createResponsavel();
		ResponsavelDto responsavelDto2 = createResponsavel();
		try {
			responsavelService.create(responsavelDto);
			responsavelService.create(responsavelDto2);
			Assert.assertTrue(false);
		} catch (ConstraintViolationException ex) {
			List<ValidatorDto> erros = getErros(ex);
			Assert.assertTrue(erros.stream().anyMatch(erro->messageService.getTexto("email.duplicado").equals(erro.getMensagem())));
		}
		
	}
	
	@Test
	public void deveCriticarDataNascimentoMenorQueDataAtual() {
		
		ResponsavelDto responsavelDto = createResponsavel();
		responsavelDto.setDataNascimento(LocalDate.now());
		try {
			responsavelService.create(responsavelDto);
			Assert.assertTrue(false);
		} catch (ConstraintViolationException ex) {
			List<ValidatorDto> erros = getErros(ex);
			Assert.assertTrue(erros.stream().anyMatch(erro->messageService.getTexto("data.erro").equals(erro.getMensagem())));
		}
		
	}
	
	

	private List<ValidatorDto> getErros(ConstraintViolationException ex) {
		List<ValidatorDto> erros = ex.getConstraintViolations().stream().map(violation -> {
			ValidatorDto validatorDto = new ValidatorDto();
			validatorDto.setMensagem(violation.getMessage());
			validatorDto.setArgs(Arrays.asList((Integer) getAttribute(violation, "max")));
			validatorDto.setErro((String) getAttribute(violation, "erro"));
			validatorDto.setLocalErro((String) getAttribute(violation, "localErro"));

			return validatorDto;
		}).collect(Collectors.toList());
		return erros;
	}
	
	@Test
	public void deveCriarUmResponsavelComSucesso() {
		Responsavel responsavelCriado = responsavelService.create(createResponsavel());
		Assert.assertNotNull(responsavelCriado.getId());
	}
	
	@Test
	public void deveDeletarUmResponsavelComSucesso() {
		Responsavel responsavelCriado = responsavelService.create(createResponsavel());
		responsavelService.deleteById(responsavelCriado.getId());
		Assert.assertNull(responsavelService.findById(responsavelCriado.getId()));
	}
	
	@Test
	public void deveEditarUmResponsavelComSucesso() {
		Responsavel responsavelCriado = responsavelService.create(createResponsavel());
		Responsavel responsavelEditado = responsavelService.edit(responsavelCriado.getId(), createResponsavelEdicao());
		
		Assert.assertEquals("Fulano 2", responsavelEditado.getNome());
		
	}
	
	@Test
	public void deveListarUmResponsavelComSucesso() {
		responsavelService.create(createResponsavel());
		ResponsavelFiltroDto filtroDto = new ResponsavelFiltroDto();
		filtroDto.setNome("Fula");
		
		List<ResponsavelDto> responsaveis = responsavelService.findAll(filtroDto);
		
		Assert.assertNotNull(responsaveis);
		Assert.assertEquals(1, responsaveis.size());
		
	}
	
	@Test
	public void deveObterUmResponsavelPorIdComSucesso() {
		Responsavel responsavel = responsavelService.create(createResponsavel());
		
		ResponsavelDto responsaveilDto = responsavelService.findById(responsavel.getId());
		
		Assert.assertNotNull(responsaveilDto);
		
	}
	
	private ResponsavelDto createResponsavelEdicao() {
		ResponsavelDto dto = new ResponsavelDto();
		dto.setCpf(CPF_VALIDO_RESPONSAVEL);
		dto.setDataNascimento(LocalDate.of(2009, 01, 01));
		dto.setEmail("teste@gmail.com");
		dto.setNome("Fulano 2");
		return dto;
	}
	
	private ResponsavelDto createResponsavel() {
		ResponsavelDto dto = new ResponsavelDto();
		dto.setCpf(CPF_VALIDO_RESPONSAVEL);
		dto.setDataNascimento(LocalDate.of(2010, 01, 01));
		dto.setEmail("testejunit@gmail.com");
		dto.setNome("Fulano");
		return dto;
	}
	
	private Object getAttribute(ConstraintViolation<?> violation, String atributo) {
		return violation.getConstraintDescriptor().getAttributes().get(atributo);
	}

}

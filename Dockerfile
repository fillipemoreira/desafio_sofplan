FROM openjdk:8-jdk-alpine

ADD target/GestaoProcessos-0.0.1-SNAPSHOT.jar app.jar

CMD ["java", "-Dspring.profiles.active=external", "-jar",  "app.jar"]
EXPOSE 8090